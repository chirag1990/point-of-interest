//
//  ViewController.swift
//  POI YOTA
//
//  Created by Tailor, Chirag on 23/03/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PoiViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    var locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    var searchResults = [MKMapItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        locationManager.delegate = self
        configureLocationServices()
        populateNearByPlaces()
        PoiTableViewCell.registerNib(tableView: tableView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func userLocationPressed(_ sender: Any) {
        centerMapOnUserLocation()
        mapView.selectAnnotation(mapView.userLocation, animated: true)
    }
    
    func populateNearByPlaces() {
        
        var region = MKCoordinateRegion()
        region.center = CLLocationCoordinate2D(latitude: self.mapView.userLocation.coordinate.latitude, longitude: self.mapView.userLocation.coordinate.longitude)
        
        guard let coordinate = locationManager.location?.coordinate else {
            //show alert
            return
        }
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = SEARCH_QUERY
        request.region = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: REGION_RADIUS * 2.0, longitudinalMeters: REGION_RADIUS * 2.0)
        
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            //print(response?.mapItems)
            guard let response = response else {
                return
            }
            
            for item in response.mapItems {
                let annotation = MKPointAnnotation()
                annotation.coordinate = item.placemark.coordinate
                annotation.title = item.name
                
                self.geocode(latitude: item.placemark.coordinate.latitude, longitude: item.placemark.coordinate.longitude) { placemark, error in
                    guard let placemark = placemark, error == nil else {
                        //show alert
                        return
                    }
                    
                    let thoroughfare = placemark.thoroughfare ?? ""
                    let subThoroughfare = placemark.subThoroughfare ?? ""
                    let locality = placemark.locality ?? ""
                    let administrativeArea = placemark.administrativeArea ?? ""
                    let postalCode = placemark.postalCode ?? ""
                    let country = placemark.country ?? ""
                    let phoneNumber = item.phoneNumber ?? ""
                    annotation.subtitle = thoroughfare + ", " + subThoroughfare + ", " + locality + ", " + administrativeArea + ", " + postalCode + ", " + country + ", " + phoneNumber
                }
                
                DispatchQueue.main.async {
                    self.mapView.addAnnotation(annotation)
                    self.searchResults = response.mapItems
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getDistanceToCoffeeShop(lat: Double, long: Double) -> Double {
        let coffeeShopCoordinate = CLLocation(latitude: lat, longitude: long)
        let userLocation = CLLocation(latitude: self.mapView.userLocation.coordinate.latitude, longitude: self.mapView.userLocation.coordinate.longitude)
        
        let distance = coffeeShopCoordinate.distance(from: userLocation)
        
        return distance
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
}

extension PoiViewController : UITableViewDelegate {
    
}

extension PoiViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PoiTableViewCell.CellConstant.Height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: PoiTableViewCell.CellConstant.Identifier) as? PoiTableViewCell {
            cell.poiNameLabel.text = searchResult.name
            let distance = getDistanceToCoffeeShop(lat: searchResult.placemark.coordinate.latitude, long: searchResult.placemark.coordinate.longitude)
            var rounderDistance = Double(round(distance*1000)/1000)
            
            if (rounderDistance >= 1609) {
                //above mile
                rounderDistance = rounderDistance/1609
                let modifiedDistance = String(format: DISTANCE_DECIMAL, rounderDistance)
                cell.poiDistanceLabel.text = "\(modifiedDistance) \(MILES)"
            } else {
                // below mile
                let modifiedDistance =  String(format: DISTANCE_DECIMAL, rounderDistance)
                cell.poiDistanceLabel.text = "\(modifiedDistance) \(METERS)"
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        centerOnSelectedPlace(indexpathRow: indexPath.row)
    }
}

extension PoiViewController : MKMapViewDelegate {
    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else {
            //show alert
            return
        }
        let coordinateRegion = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: REGION_RADIUS * 2.0, longitudinalMeters: REGION_RADIUS * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func centerOnSelectedPlace(indexpathRow : Int) {
        let searchResult = searchResults[indexpathRow]
        for annotation in mapView.annotations
        {
            if let annot = annotation.title {
                if searchResult.name == annot {
                    let latDelta:CLLocationDegrees = 0.0001
                    let lonDelta:CLLocationDegrees = 0.001
                    let span = MKCoordinateSpan.init(latitudeDelta: latDelta, longitudeDelta: lonDelta)
                    let location = CLLocationCoordinate2DMake(searchResult.placemark.coordinate.latitude, searchResult.placemark.coordinate.longitude)
                    let region = MKCoordinateRegion.init(center: location, span: span)
                    mapView.setRegion(region, animated: true)
                    mapView.selectAnnotation(annotation, animated: true)
                }
            } else {
                // show alert
            }
        }
    }
}

extension PoiViewController: CLLocationManagerDelegate {
    //Check if user has authorised app to use location service
    func configureLocationServices() {
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else {
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
    }
}
