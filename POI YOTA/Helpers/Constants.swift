//
//  Constants.swift
//  POI YOTA
//
//  Created by Tailor, Chirag on 23/03/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation

//XIBs File Constants
let POI_CELL_IDENTIFIER = "PoiCell"


//Map Constants
let REGION_RADIUS: Double = 1000

// Search String
let SEARCH_QUERY = "Coffee"

//Decimal to show in distance
let DISTANCE_DECIMAL = "%.2f"
let MILES = "Miles"
let METERS = "Meters"

