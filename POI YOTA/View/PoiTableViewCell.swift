//
//  PoiTableViewCell.swift
//  POI YOTA
//
//  Created by Tailor, Chirag on 23/03/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class PoiTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var poiDistanceIconImage: UIImageView!
    @IBOutlet weak var poiDistanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PoiTableViewCell {
    struct CellConstant {
        static let Identifier = POI_CELL_IDENTIFIER
        static let Height:CGFloat = 75
    }
    
    static func registerClass(tableView: UITableView) {
        tableView.register(PoiTableViewCell.self, forCellReuseIdentifier: CellConstant.Identifier)
    }
    
    static func registerNib(tableView:UITableView) {
        tableView.register(UINib(nibName: "PoiTableViewCell", bundle: nil), forCellReuseIdentifier: CellConstant.Identifier)
    }
    
}
